This project is created to demostrate an aspnet application deployment to VM's using docker

# Incluedes

* GitLab runner config file, running as docker with a docker image in a priviledge mode
* A docker file to build image of sample aspnet application
* GitLab ci config file which runs the job of creating docker image, pushing the image to gitlab container repository and deploying it to a VM.


# Some steps taken are here:

* Create a VM in digital ocean cloud
* Install docker on VM
* Install gitlab runner using docker as executer with docker:stable as image as priviledge
* Create a WebApi project
* Run dotnet build and run command on local machine
* Create a .dockerfile to perform
 * Restore the project dendencies ``` dotnet restore ```
 * Publish the dotnet WebApi library ``` dotnet publish -c Release -o out ```
 * Create an image using dotnet runtime and publsihed library 
 
    ``` 
       FROM microsoft/dotnet:2.1-aspnetcore-runtime AS runtime
       WORKDIR /app
       COPY --from=build /app/aspnetapp/out ./
       ENTRYPOINT ["dotnet", "dotnetapp.dll"]
    ```

* Create a .gitlab-ci.yml file and create a job to
 * Build the docker image
 * Create a tagged image to be pushed to container repository
 * Login to container repository
 * Push the image to repository